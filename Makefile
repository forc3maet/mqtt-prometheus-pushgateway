# See:
# http://www.gnu.org/software/make/manual/make.html
# http://linuxlib.ru/prog/make_379_manual.html

# Set shell interpreter
SHELL := /bin/bash

# Local env

up:
	docker-compose -f docker-compose.prod.yaml down && \
	docker-compose -f docker-compose.prod.yaml build && \
	docker-compose -f docker-compose.prod.yaml up -d

down:
	docker-compose -f docker-compose.prod.yaml down

local:
	docker-compose down && \
	docker-compose build && \
	docker-compose up -d

restart:
	docker-compose stop && \
	docker-compose up -d
