package main

import (
    "os"
	"log"
    "flag"
    "strings"
    "strconv"
    "net/http"
    "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/push"
	"github.com/prometheus/client_golang/prometheus/promhttp"
    MQTT "github.com/eclipse/paho.mqtt.golang"
)

var devices = []string {
    "hhccjcy01", "lywsd03mmc",
}

var mapper map[int]string = map[int]string {
    1: "C4:7C:8D:6B:F3:A7",
    2: "C4:7C:8D:6D:80:07",
    3: "C4:7C:8D:6D:7E:7E",
    4: "C4:7C:8D:6D:80:9A",
    5: "80:EA:CA:89:1F:56",
    6: "80:EA:CA:89:17:54",
    7: "C4:7C:8D:6A:A4:3D",
}

// define prometheus metric
var miflora = prometheus.NewGauge(
    prometheus.GaugeOpts{
        Name: "miflora_sensor",
        Help: "Xiaomi MiFlora sensor data.",
})

func init() {
    prometheus.MustRegister(miflora)
}

func main() {
    // configure http servier
	reg := prometheus.NewRegistry()
    http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))

    // get mqtt config
	topic       := flag.String("topic", "", "The topic name to/from which to publish/subscribe")
    prometheus  := flag.String("prometheus", "", "To address of the Prometheus Push Gateway")
	broker      := flag.String("broker", "tcp://host.docker.internal:1883", "The broker URI. ex: tcp://10.10.1.1:1883")
	password    := flag.String("password", "", "The password (optional)")
	user        := flag.String("user", "", "The User (optional)")
	id          := flag.String("id", "", "The ClientID (optional)")
    flag.Parse()

    // make sure variable valid
    if *topic == "" {
		log.Fatal("Invalid setting for -topic, must not be empty")
		return
	}

    if *prometheus == "" {
        log.Fatal("Invalid setting for -prometheus, must not be empty")
        return
    }

    // define configs
	opts := MQTT.NewClientOptions()
	opts.AddBroker(*broker)
	opts.SetClientID(*id)
	opts.SetUsername(*user)
	opts.SetPassword(*password)

    // start connection
    channel := make(chan [2]string)

    opts.SetDefaultPublishHandler(func(client MQTT.Client, msg MQTT.Message) {
        channel <- [2]string{msg.Topic(), string(msg.Payload())}
    })

    connection := MQTT.NewClient(opts)
    if token := connection.Connect(); token.Wait() && token.Error() != nil {
        panic(token.Error())
    }
    defer connection.Disconnect(250)

    if token := connection.Subscribe(*topic, byte(0), nil); token.Wait() && token.Error() != nil {
        log.Print(token.Error())
        os.Exit(1)
    }

    go func() {
        // wait for message
        incoming := <-channel

        // get metric labels from message
        // mushrooms/sensor/hhccjcy01_temperature/state 22.7
        message := incoming[0]
        data := strings.Split(strings.Split(message, "/")[2], "_")
        sensor, measure := data[0], data[1]
        
        value, err := strconv.ParseFloat(incoming[1], 32)
        if err != nil {
            log.Print("Cannot convert data")
        }
        miflora.Set(value)

        // push message
        err = push.New(*prometheus, "miflora_sensor").
            Collector(miflora).
            Grouping("sensor", sensor).
            Grouping("measure", measure).
            Push()
        if err != nil {
            log.Print("Could not push completion time to Pushgateway:", err)
        }
        log.Print("Got new message from ", sensor)
    }()

    // start http
    log.Print("Serve and listen")
	log.Fatal(http.ListenAndServe(":8080", nil))
}